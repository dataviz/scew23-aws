"""Facility coverage

TODO: documentation
TODO: try out different versions of streamlit cache
TODO: Display a message if pois_df is empty (no amenities found)
"""

import streamlit as st
import base64
from PIL import Image
from src import config, lang

from src.isochrones import PrecomputedIsochrones
from src.map import plot_map_from_gdf
from streamlit_folium import st_folium
from st_clickable_images import clickable_images
from streamlit_js_eval import streamlit_js_eval
from src.utils import hide_img_fs, hide_streamlit_style, hide_deploy_button

st.set_page_config(
    page_title="15min",
    page_icon=":cityscape:",
    layout="wide",
    initial_sidebar_state="expanded",
    menu_items={
        "Get Help": "https://www.bsc.es/viz",
        "Report a bug": "https://www.bsc.es/viz",
        "About": (
            "[Data Analytics and Visualization Team](https://bsc.es/viz),"
            " [Barcelona Supercomputing Center](https://bsc.es), BSC."
        ),
    },
)

iso_barna = PrecomputedIsochrones("barna")
iso_kobe = PrecomputedIsochrones("kobe")

st.session_state.setdefault("place", config.DEFAULT_LOCATION_STR)
st.session_state.setdefault("facility_type", config.DEFAULT_FACILITY_TYPE)
st.session_state.setdefault("minutes", config.DEFAULT_MINUTES)
st.session_state.setdefault("transport_mode", "foot-walking")
st.session_state.setdefault("is_uploaded_file", False)
st.session_state.setdefault("logo_filename", "badge-barna.png")

# Save viewport width to session state
st.session_state["viewport_width"] = streamlit_js_eval(
    js_expressions="window.innerWidth", key="ViewportWidth"
)

st.markdown(hide_img_fs, unsafe_allow_html=True)
st.markdown(hide_streamlit_style, unsafe_allow_html=True)
st.markdown(hide_deploy_button, unsafe_allow_html=True)


def set_uploaded_file_true():
    st.session_state["is_uploaded_file"] = True


def set_uploaded_file_false():
    st.session_state["is_uploaded_file"] = False


def change_ajuntament_logo():
    if st.session_state["place"] == config.DEFAULT_LOCATION_STR:
        st.session_state["logo_filename"] = "badge-barna.png"
    else:
        st.session_state["logo_filename"] = "badge-kobe.png"


def set_logo():
    file_name = st.session_state["logo_filename"]
    return st.image(Image.open(f"static/images/{file_name}"), use_column_width=True)


def change_city_to_kobe():
    st.session_state["place"] = _("Kobe")
    change_ajuntament_logo()


def change_city_to_barcelona():
    st.session_state["place"] = _("Barcelona")
    change_ajuntament_logo()

st.markdown(
    """
    <style>
        [data-testid=stSidebar] {
            background-color: #262730;
            color: #ffffff;
            text-align: right;
            width:500px;
        }
        .sidebar .sidebar-content {
        display: flex;
        flex-direction: column;
        align-items: center;
    }
    </style>
    """,
    unsafe_allow_html=True,
)
with st.sidebar:
    st.markdown(
        "<h1 style='text-align: right; color: white; font-size: 400%;'>Proximity cities</h1>",
        unsafe_allow_html=True,
    )

    st.markdown(
        """
        <style>
            button[kind="primary"] {
                background-color: #262730;
                color: #ffffff;
                border: none;
                font-size: 48px;
                width: 75%;
                margin-right: 0%;
                display: inline-block;
                height: 100px;
                border-radius: 25px 0px 0px 25px;
            }
            button[kind="primary"]:hover {
                background-color: #ffffff;
                color: #262730;
            }
        </style>
    """,
        unsafe_allow_html=True,
    )

    st.button(
        label="Barcelona",
        on_click=change_city_to_barcelona,
        type="primary",
    )
    st.button(label="Kobe", on_click=change_city_to_kobe, type="primary")

    st.markdown(
        """
        <style>
            div[data-baseweb="select"] > div {
                background-color: #262730;
                color: #ffffff;
            }
            .stSelectbox{
                padding-left: 12rem;
                padding-right: 3rem;
                padding-bottom: 100%
            }
            ul.st-c1 > div:nth-child(1) > div:nth-child(1) {
                background-color: #262730;
                color: #ffffff;
                item-align: right;
            }
            .st-bf {
                background-color: #262730;
                color: #ffffff;
            }
            .st-bt {
                background-color: #262730;
                color: #ffffff;
            }
    """,
        unsafe_allow_html=True,
    )

    language = st.sidebar.selectbox(
        label="Language",  # TODO: Use the translation stuff for the label
        options=list(lang.language_from_code.keys()),
        index=list(lang.language_from_code.keys()).index(config.DEFAULT_LANGUAGE),
        format_func=lambda code: lang.language_from_code[code],
    )

    _ = lang.init_translator(language)
    facility_type_translation = lang.get_facility_types()
    transport_modes_available_translation = lang.get_available_transport_modes()
    sentence_transport_modes = lang.get_sentence_transport_modes()

    st.image("static/images/icons/LOGO.png", use_column_width=True)


st.markdown(
    """
        <style>
            .appview-container .main .block-container {{
                padding-top: 0rem;
                padding-bottom: 0rem;
                }}

        </style>""".format(
        padding_top=1, padding_bottom=1
    ),
    unsafe_allow_html=True,
)  # https://github.com/streamlit/streamlit/issues/6336

col_head_1, col_head_2 = st.columns([0.8, 0.2])
with col_head_1:
    st.title(
        _("**Is** {city} **a proximity city?**").format(city=st.session_state["place"])
    )

st.markdown(
    """
        <style>
            [data-testid=stImage] {
                margin-left: auto;
                margin-top:1rem;
            }
            div.css-ocqkz7:nth-child(4) > div:nth-child(2) {
                width: 5rem;
            }
        </style>
    """,
    unsafe_allow_html=True,
)
with col_head_2:
    set_logo()

container_question = st.empty()
container_explanation = st.empty()


def refresh_intro():
    if not st.session_state["is_uploaded_file"]:
        container_explanation.markdown(
            _(
                # TRANSLATORS: Please keep asterisks and brackets. Please do not translate words within the curly brackets.
                """The map shows the areas in **{city}** where a facility of type **{facility_type}** can be reached within **{minutes}** minutes {sentence_transport_mode}."""
            ).format(
                city=st.session_state["place"],
                facility_type=facility_type_translation[
                    st.session_state["facility_type"]
                ],
                minutes=st.session_state["minutes"],
                sentence_transport_mode=sentence_transport_modes[
                    st.session_state["transport_mode"]
                ],
            )
        )
    else:
        container_question.markdown(_("Are the facilities properly distributed?"))

        container_explanation.markdown(
            _(
                # TRANSLATORS: Please keep asterisks and brackets. Please do not translate words within the curly brackets.
                """The map shows the areas in which the facilities can be reached within **{minutes}** minutes {sentence_transport_mode}."""
            ).format(
                minutes=st.session_state["minutes"],
                sentence_transport_mode=sentence_transport_modes[
                    st.session_state["transport_mode"]
                ],
            )
        )


col1, col2, col3 = st.columns([0.45, 0.35, 0.20])

with col1:
    st.markdown(_("**Facility type**"))
    facilities = ["school", "library", "police", "fire-stations"]

    images_encoded = []
    for f in facilities:
        if f == st.session_state["facility_type"]:
            with open(
                f"static/images/icons/facility-types/{f}-selected.png", "rb"
            ) as image:
                encoded = base64.b64encode(image.read()).decode()
                images_encoded.append(f"data:image/png;base64,{encoded}")
        else:
            with open(f"static/images/icons/facility-types/{f}.png", "rb") as image:
                encoded = base64.b64encode(image.read()).decode()
                images_encoded.append(f"data:image/png;base64,{encoded}")

    click_facility = clickable_images(
        images_encoded,
        titles=[_("school"), _("library"), _("police"), _("fire station")],
        div_style={"display": "flex", "flex-wrap": "wrap"},
        img_style={"margin": "5px"},
    )

    if click_facility > -1:
        st.session_state["facility_type"] = facilities[click_facility]

with col2:
    transports = ["foot-walking", "cycling-regular", "driving-car"]

    st.markdown(_("**Transport mode**"))
    images_encoded = []
    for t in transports:
        if t == st.session_state["transport_mode"]:
            with open(
                f"static/images/icons/transport-types/{t}-selected.png", "rb"
            ) as image:
                encoded = base64.b64encode(image.read()).decode()
                images_encoded.append(f"data:image/png;base64,{encoded}")
        else:
            with open(f"static/images/icons/transport-types/{t}.png", "rb") as image:
                encoded = base64.b64encode(image.read()).decode()
                images_encoded.append(f"data:image/png;base64,{encoded}")

    click_transport = clickable_images(
        images_encoded,
        titles=[_("walking"), _("cycling"), _("driving")],
        div_style={"display": "flex", "flex-wrap": "wrap"},
        img_style={"margin": "5px"},
    )

    if click_transport > -1:
        st.session_state["transport_mode"] = list(
            transport_modes_available_translation.keys()
        )[click_transport]


with col3:
    st.session_state["minutes"] = st.slider(
        min_value=5,
        step=5,
        max_value=20,
        value=15,
        label=_("Maximum time to reach a facility (in minutes)"),
    )

refresh_intro()

# set isochrone engine
if st.session_state["place"] == "Kobe":
    pois_isochrones_gdf = iso_kobe.get_isochrones(
        amenity=st.session_state["facility_type"],
        transport=st.session_state["transport_mode"],
        time=st.session_state["minutes"],
    )

else:
    pois_isochrones_gdf = iso_barna.get_isochrones(
        amenity=st.session_state["facility_type"],
        transport=st.session_state["transport_mode"],
        time=st.session_state["minutes"],
    )

# call to render Folium map in Streamlit
make_map_responsive = """
    <style>
        [title~="st.iframe"] { width: 100%, align: center}
    </style>
"""
st.markdown(
    make_map_responsive, unsafe_allow_html=True
)  # https://github.com/randyzwitch/streamlit-folium/issues/7

m = plot_map_from_gdf(pois_isochrones_gdf=pois_isochrones_gdf, tiles=config.MAP_TILES)

st_folium(
    m,
    width="100%",
    returned_objects=None,
)  # https://github.com/randyzwitch/streamlit-folium/blob/83eec9e826bc0f2d3a2f2fa56772a54df4954360/examples/pages/static_map.py


st.markdown(
    """
    <style>
        div[data-testid="column"]:nth-of-type(1)
        {
            text-align: start;
        } 

        div[data-testid="column"]:nth-of-type(2)
        {
            text-align: end;
        } 
    </style>
    """
    , unsafe_allow_html=True
)
end_col1, end_col2 = st.columns(2)

with end_col1:
    st.caption(iso_barna.markdown_message_info)

with end_col2:
    # TODO: Align text to the left
    st.caption(
        _(
            "Data Analytics and Visualization Team, Barcelona Supercomputing Center, BSC."
        )
    )
