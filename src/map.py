import folium
import geopandas as gpd
import numpy as np
import streamlit as st
from src import config


def plot_amenity(row, crs: gpd.GeoDataFrame, isochrone_map: folium.Map):
    folium.features.GeoJson(
        gpd.GeoSeries(row["isochrones_polygons"], crs=crs).__geo_interface__,
        style_function=lambda x: {
            "fillColor": config.COLOR,
            "color": config.COLOR,
            "weight": config.ISOCHRONE_LINE_WEIGHT,
        },
    ).add_to(isochrone_map)

    folium.map.Marker(
        (
            row["geometry"].y,
            row["geometry"].x,
        ),  # reverse coords due to weird folium lat/lon syntax
        icon=folium.Icon(
            color=config.MARKER_COLOR,
            icon_color=config.ICON_COLOR,
            icon=config.ICON,
            prefix="fa",
        ),
        popup=row["name"],
    ).add_to(isochrone_map)


def plot_map_from_gdf(
    pois_isochrones_gdf,
    tiles="cartodbpositron",
    location=None,
    zoom_start=12,
    control_scale=True,
):
    """
    Return a folium map with the isochones of every POI/row in the gdf.

    Notes:
    * By default, folium.Map uses crs='EPSG3857'.
      https://python-visualization.github.io/folium/modules.html#module-folium.folium
      https://github.com/python-visualization/folium/issues/1161#issuecomment-502009739
    """
    pois_isochrones_gdf = pois_isochrones_gdf.to_crs("epsg:4326")
    pois_isochrones_gdf["isochrones_polygons"] = pois_isochrones_gdf[
        "isochrones_polygons"
    ].to_crs("epsg:4326")
    crs = pois_isochrones_gdf.crs.to_epsg(3857)

    if not location:
        centroids = pois_isochrones_gdf.to_crs("epsg:3857").centroid.to_crs(
            pois_isochrones_gdf.crs.to_epsg()
        )
        location = (centroids.y.mean(), centroids.x.mean())
        if any(np.isnan(location)):
            st.error(("Cannot center the map. Are there any facilities?"))
            location = (config.DEFAULT_LOCATION_LAT, config.DEFAULT_LOCATION_LON)

    # Set up folium map
    isochrone_map = folium.Map(
        tiles=tiles,
        location=location,
        zoom_start=zoom_start,
        control_scale=control_scale,
    )

    pois_isochrones_gdf.apply(lambda x: plot_amenity(x, crs, isochrone_map), axis=1)

    return isochrone_map


def custom_isochrone():
    pass
