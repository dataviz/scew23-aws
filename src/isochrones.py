from dataclasses import dataclass
from os import listdir

import geopandas as gpd
from openrouteservice import Client, isochrones, exceptions
from shapely.geometry import Point
from src.utils import get_osm_amenities


class IsochroneEngine(object):
    __dict__ = {"client", "city"}

    def __init__(self, location: str = "Barcelona"):
        if location == "Barcelona":
            self.client = Client(base_url="http://localhost:8080/ors")
            self.city = "Barcelona"

        elif location == "Kobe":
            self.client = Client(base_url="http://localhost:8081/ors")
            self.city = "Kobe"

        else:
            raise ValueError("Invalid city")

    def compute_isochrone(self, row, iso_profile: str, time: int) -> gpd.GeoDataFrame:
        """
        Retrieve an isochrone from a Point

        :param location: Position
        :param iso_profile: Routing profile
        :param time: Isochrone radius (minutes)
        :return: Isochrone dataframe
        """

        try:
            res = isochrones.isochrones(
                self.client,
                [(row.x, row.y)],
                iso_profile,
                range_type="time",
                range=[time * 60],
                smoothing=0.8,
            )

            return gpd.GeoDataFrame.from_features(res["features"], crs="EPSG:4326")[
                "geometry"
            ].values[0]

        except Exception:
            # Point not in current map
            return None

    # @st.cache_data
    def get_isochrones(
        self,
        iso_profile: str,
        amenity: str,
        time: int,
        pois_gdf: gpd.GeoDataFrame = None,
    ) -> gpd.GeoDataFrame:
        """
        Get isochrones from OpenRouteService API
        :param iso_profile: profile of isochrones
        :param amenity: OpenStreetMap amenity
        :param time: Isochrone time radius (minutes)
        :param pois_gdf: GeoDataFrame with POIs
        :return: GeoDataFrame with isochrones
        """

        # Get POIs from OpenStreetMap
        if pois_gdf is None:
            pois_gdf = get_osm_amenities(self.city, amenity)
            pois_gdf.set_geometry("geometry", inplace=True)

        isochrones_poly = pois_gdf.geometry.apply(
            self.compute_isochrone, iso_profile=iso_profile, time=time
        )

        isochrones_poly.dropna(inplace=True)
        isochrones_poly = isochrones_poly.to_crs("EPSG:4326")

        return isochrones_poly

    @property
    def markdown_message_info(self) -> str:
        return _("Isochrones do not take into account altitude.")


@dataclass
class PrecomputedIsochrones(object):
    """Store precomputed isochrones"""

    data: dict

    def __init__(self, location: str = "barna"):
        self.data = {
            file.split(".")[0]: gpd.read_parquet(f"static/isochrones/{location}/{file}")
            for file in listdir(f"static/isochrones/{location}")
        }

    def get_isochrones(
        self, amenity: str, transport: str, time: int
    ) -> gpd.GeoDataFrame:
        """
        Return isochrones for defined amenity, transport and time
        :param amenity: Amenity
        :param transport: Transport
        :param time: Time
        :return: Isochrones
        """
        return self.data[amenity][["name", "geometry", f"{transport}_{time}"]].rename(
            columns={f"{transport}_{time}": "isochrones_polygons"}
        )

    @property
    def markdown_message_info(self) -> str:
        return _("Isochrones do not take into account altitude.")
