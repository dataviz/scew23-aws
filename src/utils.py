import geopandas as gpd
import osmnx as ox
import streamlit as st

hide_img_fs = """
<style>
button[title="View fullscreen"]{
    visibility: hidden;}
</style>
"""

hide_streamlit_style = """
            <style>
            #MainMenu {visibility: hidden;}
            footer {visibility: hidden;}
            </style>
            """

hide_deploy_button = """
    <style>
        .reportview-container {
            margin-top: -2em;
        }
        #MainMenu {visibility: hidden;}
        .stDeployButton {display:none;}
        footer {visibility: hidden;}
        #stDecoration {display:none;}
    </style>
"""


def get_osm_amenities(location: str, amenity: str) -> gpd.GeoDataFrame:
    """
    Get Point of Interests (POIs) from OpenStreetMap API
    :param location: city name
    :param amenity: type of amenity from OpenStreetMaps
    :return: GeoDataFrame with POIs
    """

    amenities = ox.features_from_place(location, tags={"amenity": amenity})
    amenities = amenities.to_crs("EPSG:4326")

    # transform geometry polygon to point
    pois_gdf = amenities[["name", "geometry"]]
    pois_gdf = pois_gdf.set_geometry(pois_gdf.centroid, crs="epsg:4326")
    pois_gdf = pois_gdf.reset_index()[["name", "geometry"]]

    pois_gdf.dropna(inplace=True)

    return pois_gdf


def local_css(file_name):
    with open(file_name) as f:
        st.markdown(f"<style>{f.read()}</style>", unsafe_allow_html=True)


def remote_css(url):
    st.markdown(f'<link href="{url}" rel="stylesheet">', unsafe_allow_html=True)
