"""POIs formatter, from CSV, OpenStreetMap.
Convert to DataFrame (name, lat, lon)

TODO: Refactor to a GeoDataFrame subclass, or similar
"""

import re
import urllib

import geopandas as gpd
import numpy as np
import osmnx as ox
import pandas as pd
import pandera as pa
import streamlit as st
from pandera.typing import Series
from pandera.typing.geopandas import GeoDataFrame, GeoSeries
from src import config

OSM_AMENITY_VALUES_URL = "https://wiki.openstreetmap.org/wiki/Key:amenity#Values"


class POIs_Schema(pa.SchemaModel):
    name: Series[str] = pa.Field(nullable=True)
    geometry: GeoSeries = pa.Field(nullable=False)
    isochrones_polygons: GeoSeries = pa.Field(nullable=True)


class POIs_Isochrones_Schema(pa.SchemaModel):
    name: Series[str] = pa.Field(nullable=False)
    geometry: GeoSeries = pa.Field(nullable=False)
    isochrones_polygons: GeoSeries = pa.Field(nullable=False)


class POIsFormatter:
    @staticmethod
    @pa.check_types
    def empty_gdf() -> GeoDataFrame[POIs_Schema]:
        return gpd.GeoDataFrame(
            columns=["name", "geometry", "isochrones_polygons"], crs=config.CRS
        ).assign(isochrones_polygons=gpd.GeoSeries(crs=config.CRS))

    @staticmethod
    @pa.check_types
    def from_csv(filename) -> GeoDataFrame[POIs_Schema]:
        df = pd.read_csv(filename)
        pois_gdf: gpd.GeoDataFrame = gpd.GeoDataFrame(
            df, geometry=gpd.points_from_xy(df["lon"], df["lat"], crs=config.CSV_CRS)
        ).to_crs(config.CRS)
        pois_gdf = pois_gdf.assign(isochrones_polygons=gpd.GeoSeries(crs=config.CRS))
        pois_gdf = pois_gdf.drop(columns=["lat", "lon"])
        return pois_gdf

    @staticmethod
    @pa.check_types
    @st.cache_data
    def from_osm(
        place: str = "Barcelona, Spain", amenity_type: str = "hospital"
    ) -> GeoDataFrame[POIs_Schema]:
        """Get Point of Interests (POIs) from OpenStretMap API

        Args:
            place (str, optional): The location/city to query. Defaults to "Barcelona, Spain".
            amenity_type (str, optional): Type of amenity from OpenStreetMaps. Defaults to "hospital".
        Returns:
            gpd.GeoDataFrame: GeoDataframe with columns "name" and "geometry" per every POI.
                              A empty column "isochrones_polygons" is also included.
        """

        try:
            gdf = ox.geometries.geometries_from_place(
                place,
                tags={
                    "amenity": amenity_type,
                },
            )
        except ValueError as err:
            gdf = POIsFormatter.empty_gdf()

        gdf = gdf.to_crs(config.CRS)

        try:
            pois_gdf = gdf[["name", "geometry"]]  # .copy(deep=True)
        except KeyError:
            pois_gdf = POIsFormatter.empty_gdf()

        pois_gdf = pois_gdf.assign(isochrones_polygons=gpd.GeoSeries(crs=config.CRS))

        # transform geometry polygon to point
        pois_gdf = pois_gdf.set_geometry(pois_gdf.centroid)

        pois_gdf = pois_gdf.reset_index()[["name", "geometry", "isochrones_polygons"]]

        return pois_gdf

    @staticmethod
    def to_df(pois_gdf: gpd.GeoDataFrame) -> pd.DataFrame:
        """Convert POIs GeoDataFrame in a DataFrame with lat/lon.

        Args:
            pois_gdf (gpd.GeoDataFrame): GeoDataFrame with name, geometry and isochrones_polygons

        Returns:
            pd.DataFrame: DataFrame with name, lat, lon
        """
        pois_gdf = pois_gdf.to_crs(config.CSV_CRS)
        pois_gdf["lon"] = pois_gdf["geometry"].x
        pois_gdf["lat"] = pois_gdf["geometry"].y
        return pois_gdf[["name", "lat", "lon"]]


@st.cache_data
def get_facility_types_from_osm_wiki(
    osm_wiki_url: str = OSM_AMENITY_VALUES_URL,
) -> tuple:
    """
    TODO: Improve urllib exception and default returning tuple
    """
    tag_amenity_regex = """Tag:amenity=([^\"]*)"""
    pattern = re.compile(tag_amenity_regex)

    try:
        htmlfile = urllib.request.urlopen(OSM_AMENITY_VALUES_URL)
    except urllib.error.HTTPError as err:
        if config.DEBUG:
            st.error("HTTPError: {err}")
        return ("hospital", "school")
    except Exception:
        if config.DEBUG:
            st.error("Exception: {err}")
        return ("hospital", "school")

    htmltext = htmlfile.read().decode("utf8")
    titles = re.findall(pattern, htmltext)
    return tuple(sorted({t for t in titles if "(page does not exist)" not in t}))


def prettify_df(df: pd.DataFrame, advanced=False) -> pd.DataFrame:
    pretty_df = df.copy()
    pretty_df.index = np.arange(1, len(pretty_df) + 1)
    pretty_df.rename(
        columns={"name": _("name"), "lat": _("lat"), "lon": _("lon")}, inplace=True
    )
    if not advanced:
        pretty_df = pretty_df[[_("name")]]
    return pretty_df
