import os

import streamlit as st

DEBUG = os.getenv("DEBUG") in ["1", "True", "true", "TRUE"]

DEFAULT_LANGUAGE = os.getenv("DEFAULT_LANGUAGE", "en")

FACILITY_TYPE_LIST = [
    "atm",
    "bank",
    "bench",
    "cinema",
    "clinic",
    "college",
    "fountain",
    "fire station",
    "hospital",
    "kindergarten",
    "library",
    "marketplace",
    "parking",
    "pharmacy",
    "police",
    "recycling",
    "school",
    "shelter",
    "taxi",
    "theatre",
    "toilets",
    "townhall",
    "university",
    "veterinary",
]

CRS = "EPSG:3857"
CSV_CRS = "EPSG:4326"

DEFAULT_LOCATION_STR = os.getenv("DEFAULT_LOCATION_STR", "Barcelona")
DEFAULT_LOCATION_LAT = float(os.getenv("DEFAULT_LOCATION_LAT", "41.38879"))
DEFAULT_LOCATION_LON = float(os.getenv("DEFAULT_LOCATION_LON", "2.15899"))

DEFAULT_FACILITY_TYPE = os.getenv("DEFAULT_FACILITY_TYPE", "library")
DEFAULT_TRANSPORT_MODE = os.getenv("DEFAULT_TRANSPORT_MODE", "foot-walking")
DEFAULT_MINUTES = int(os.getenv("DEFAULT_MINUTES", "15"))

NETWORK_TYPE = "walk"
TRAVEL_SPEED = 4.5  # walking speed in km/hour
METERS_PER_MINUTE = TRAVEL_SPEED * 1000 / 60  # km per hour to m per minute

ISOCHRONE_ENGINE = os.getenv("ISOCHRONE_ENGINE", "ORS")

ORS_SERVER_URL = os.getenv("ORS_SERVER_URL", "http://localhost:8080/ors")
ORS_TOKEN = None if os.getenv("ORS_TOKEN", "") == "" else os.getenv("ORS_TOKEN", None)
ORS_MAX_N_ISOCHRONES_PER_PETITION = int(
    os.getenv("ORS_MAX_N_ISOCHRONES_PER_PETITION", "5")
)

MAP_TILES = "CartoDB positron"
MARKER_COLOR = "black"
ICON_COLOR = st.get_option(
    "theme.primaryColor"
)  # "#E7A300" "#228B22" '#00FF00' '#cc0000'
COLOR = st.get_option("theme.primaryColor")  # 'lightgreen'
ISOCHRONE_LINE_WEIGHT = 0
ICON = "circle"
